/* hw4.c */
/* 1. 受け取った要素k分、文字列をずらす機能を持つrotateを作る */
/* 2. unit test(単体テスト)を行うこと
※googletestも一応少し調べたりしたのですが、よくわからなかったので、
プログラム内で何か動作を行う(成功する)度に、逐一printfで成功を示す表示が出るようにしています。
  */

#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<errno.h>

int is_valid_word(char* word){ //入力された文字列が'a'~'z'又は'A'~'Z'であるか判断
  int i;
  for(i=0;i<strlen(word);i++){
    if(*(word+i)<'a'||*(word+i)>'z'||*(word+i)<'A'||*(word+i)>'z'){
      return -1;
    }
  }
  return 1;
}

main(int argc,char *argv[]){
/* 入力に対する処理スタート */

  if(argc != 3){      //引数が3つじゃないとき
    fprintf(stderr,"Usage: hw4 <alphabets> <number>\n");
    exit(EXIT_FAILURE);
  }
  printf("引数３つ確認。OK\n");

  if(is_valid_word(argv[1]) < 0){
    perror("Invalid words\n");
    exit(EXIT_FAILURE);
  }
  printf("２つ目の引数 %s が有効な文字(アルファベット)であることを確認。OK\n",argv[1]);

  char kotoba[50];
  strcpy(kotoba,argv[1]);

  /* 入力されたchar型の文字列としての数字をlong型に変換 */
  char *ptr;
  long k;

  k = strtol(argv[2],&ptr,10);
  if(errno == EINVAL || errno == ERANGE){
    perror("ERROR(hw4)");
    exit(EXIT_FAILURE);
  }
  else if(errno != 0){
    perror("Unexpected ERROR(hw4)");
    exit(EXIT_FAILURE);
  }
  else if(ptr == argv[2]){
    fprintf(stderr,"No number is found.\n");
    exit(EXIT_FAILURE);
  }
  printf("%s を",kotoba);
  printf("%ld文字分、rotateします。\n",k);

  if(*ptr != '\0'){    //数字の後ろに謎の文字が続いていた場合
    printf("（数字の後ろに %s　とありますが、これは無視します。）\n",ptr);
  }

  /* ここまでで入力されたもの(引数)に対する処理(代入作業)は終了 */
  /* ここからrotateの実装スタート */

  //テスト例
  //rotate("abcde",1)   → "eabcd"
  //rotate("abcde",-2)  → "cdeab"

  char rot[50];
  int i,j;
 
  if(k<0){
    k = strlen(kotoba)+k;
  }
    for(i=0; i<(strlen(kotoba)-k); i++){
      rot[i+k] = kotoba[i];
    }
    for(j=k; j>0; j--){
      rot[k-j] = kotoba[strlen(kotoba)-j];
    }
    printf("roatateした結果は、%s となった。\n",rot);
}
Yuiho Ishida
